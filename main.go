package main

import "fmt"

func main() {
	fmt.Println("Hello, world!")
	sayHello()
	sayGoodBye()
}

func sayHello() {
	fmt.Println("Hello, Yudi!")
}

func sayGoodBye() {
	fmt.Println("Good bye, Yudi dan kawan2!")
}
